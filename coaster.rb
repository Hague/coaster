#!/usr/bin/ruby

require 'open3'

ULTISNIPS_GLOB = File.join(ENV['HOME'], '/.config/nvim/UltiSnips/*.snippets')
SNIPMATE_GLOB = File.join(ENV['HOME'], '/.config/nvim/snippets/*.snippets')

SET_SYSTEM = 'wl-copy'.freeze
SET_PRIMARY = 'wl-copy --primary'.freeze

MENU_CMD = "bemenu --prompt 'Choose a paste' --ignorecase".freeze

def read_ultisnips(filename)
  snippets = {}
  begin
    current_snip_meta = ""
    current_snip_content = ""
    File.open(filename, 'r').each_line do |line|
      if line.start_with? 'snippet'
        current_snip_meta = line[("snippet".length + 1)..].chomp
        current_snip_meta += ' ::: '
        current_snip_content = ""
      elsif line.start_with? 'endsnippet'
        # strip last newline, but not all newlines
        snippets[current_snip_meta] = current_snip_content[..-2]
      else
        current_snip_content += line
        current_snip_meta += line.chomp
      end
    end
  rescue Errno::ENOENT
    # do nothing
  end
  snippets
end

def read_snipmate(filename)
  snippets = {}
  begin
    current_snip_meta = ""
    current_snip_content = ""
    File.open(filename, 'r').each_line do |line|
      if not line.match?("^\s\+") and not current_snip_meta.empty?
        # strip last newline, but not all newlines
        snippets[current_snip_meta] = current_snip_content[..-2]
        current_snip_meta =  ""
        current_snip_content = ""
      end
      if line.start_with? 'snippet'
        current_snip_meta = line[("snippet".length + 1)..].chomp
        current_snip_meta += ' ::: '
        current_snip_content = ""
      else
        line = line.lstrip()
        current_snip_content += line
        current_snip_meta += line.chomp
      end
    end
    # if reached eof with snippet
    if not current_snip_meta.empty?
      # strip last newline, but not all newlines
      snippets[current_snip_meta] = current_snip_content[..-2]
    end
  rescue Errno::ENOENT
    # do nothing
  end
  snippets
end

def read_all_snippets
  all_snippets = {}
  begin
    Dir.glob(ULTISNIPS_GLOB) do |snipsfile|
      all_snippets.merge!(read_ultisnips(snipsfile))
    end
    Dir.glob(SNIPMATE_GLOB) do |snipsfile|
      all_snippets.merge!(read_snipmate(snipsfile))
    end
  rescue Errno::ENOENT
    # do nothing
  end
  all_snippets
end

def select_snippet(snippets)
  result = ""
  Open3.popen3(MENU_CMD) do |menu_in,menu_out|
    snippets.each_key do |key|
      menu_in.write(key)
      menu_in.write("\n")
    end
    menu_in.close
    result = menu_out.gets.chomp
  end

  if snippets.member? result
    new_clip = snippets[result]
    Open3.popen3(SET_SYSTEM) do |clip_in|
      clip_in.write new_clip
    end
    Open3.popen3(SET_PRIMARY) do |clip_in|
      clip_in.write new_clip
    end
  end
end

select_snippet(read_all_snippets())
