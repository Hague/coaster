
# Coaster

A simple ruby script for make your UltiSnip or SnipMate snippets available
across your system. Uses a standard menu app (e.g. bemenu) and copies to your
clipboard for pasting anywhere. Currently set up for wayland.

## Configure

Edit the constants at the top of the script to match your system.

* `ULTISNIPS_GLOB` -- which UltiSnips snippets files to read from.
* `SNIPMATE_GLOB` -- which SnipMate snippets files to read from.
* `SET_SYSTEM` -- the command for copying to the system clipboard.
* `SET_PRIMARY` -- the command for copying to the primary clipboard.
* `MENU_CMD` -- your menu command.

By default, looks in `~/.config/nvim/UltiSnips/*.snippets` and `~/.config/nvim/snippets/*.snippets` , uses `wl-copy` and `bemenu`.

## Run

Run

    $ ruby coaster.rb

And it reads your snippets, uses your menu command to allow you to
choose, then copies to the clipboard for you to paste.

